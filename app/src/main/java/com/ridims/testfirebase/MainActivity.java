package com.ridims.testfirebase;

import android.support.v4.widget.TextViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseListAdapter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        EditText editText = (EditText) findViewById(R.id.edt_text);
        Button button = (Button) findViewById(R.id.btn_push);

        ListView mListView = (ListView) findViewById(R.id.listview);
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference();
        final DatabaseReference listRef = ref.child("nama");

        FirebaseListAdapter<String> mAdapter = new FirebaseListAdapter<String>(
                this,
                String.class,
                android.R.layout.simple_list_item_1,
                listRef
        ) {
            @Override
            protected void populateView(View v, String model, int position) {
                ((TextView) v.findViewById(android.R.id.text1)).setText(model);
            }
        };
        mListView.setAdapter(mAdapter);

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(MainActivity.this, "Test", Toast.LENGTH_SHORT).show();
            }
        });

//        new code
//        button.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                String edit = editText.getText().toString();
//
//                listRef.setValue(edit);
//                listRef.push();
//            }
//        });
    }
}
